$(function () {


    var pathArray = window.location.pathname.match("chat/")['input'];

    var res = pathArray.split("/");


    // getting the id of the room from the url
    // var id = Number(window.location.pathname.match(/\/chat\/(\d+)$/)[1]);
    id = res[2];
    // console.log(pathArray);
// var id;
    var socket = io();
    // id = '2asdasds2343ad2324234asd23=';

    socket.on('sessionId', function (data) {

        // id = data;

    });
    // store all email test
    var mailText = '';


    // variables which hold the data for each person
    var name = "",
        email = "",
        img = "",
        friend = "",
        department = "";

    // cache some jQuery objects
    var section = $(".section"),
        footer = $("footer"),
        onConnect = $(".connected"),
        inviteSomebody = $(".invite-textfield"),
        personInside = $(".personinside"),
        chatScreen = $(".chatscreen"),
        left = $(".left"),
        noMessages = $(".nomessages"),
        tooManyPeople = $(".toomanypeople");


    var typing = false;
    var lastTypingTime;
    var connected = false;
    var TYPING_TIMER_LENGTH = 400; // ms


    // some more jquery objects
    var chatNickname = $(".nickname-chat"),
        leftNickname = $(".nickname-left"),
        loginForm = $(".loginForm"),
        yourName = $("#yourName"),
        yourEmail = $("#yourEmail"),
        hisName = $("#hisName"),
        hisEmail = $("#hisEmail"),
        chatForm = $("#chatform"),
        textarea = $(".message-input"),
        messageTimeSent = $(".timesent"),
        chats = $(".chats"),
        dep = $('#dep');


    // these variables hold images
    var ownerImage = $("#ownerImage"),
        leftImage = $("#leftImage"),
        noMessagesImage = $("#noMessagesImage");


    // test


    // test
    //
    // function SendData(url, data) {
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('POST', url, true);
    //     xhr.send(data);
    // }

    // SendData("http://127.0.0.1:8080/", "Hello World, this is data!");

    // current tab
    var vis = (function () {
        var stateKey, eventKey, keys = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }
        return function (c) {
            if (c) document.addEventListener(eventKey, c);
            return !document[stateKey];
        }
    })();

    // // audio playing
    // var audioElement = document.createElement('audio');
    // audioElement.setAttribute('src', '/telegram.mp3');
    //
    // audioElement.addEventListener('ended', function() {
    //     this.play();
    // }, false);
    //
    //
    // audioElement.play();

// // end audio playing
    vis(function () {


        if (vis())
            socket.emit('notif');


    });
    socket.on('notif', function () {
        // console.log('message received!!');
    });


    //test


// Is typing
    textarea.on('input', function () {
        updateTyping();
    });

    function updateTyping() {

        if (connected) {
            if (!typing) {
                typing = true;
                socket.emit('typing', {user: name, img: img});

            }
            lastTypingTime = (new Date()).getTime();

            setTimeout(function () {
                var typingTimer = (new Date()).getTime();
                var timeDiff = typingTimer - lastTypingTime;
                if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                    socket.emit('stop typing', {user: name, img: img});
                    typing = false;
                }
            }, TYPING_TIMER_LENGTH);
        }
    }

    // Gets the 'X is typing' messages of a user
    function getTypingMessages(data) {
        return $('.typing').filter(function (i) {
            return $(this).data('user') === data.user;
        });
    }


    // Removes the visual chat typing message
    function removeChatTyping(data) {

        getTypingMessages(data).fadeOut(function () {
            $(this).remove();
        });
    }

    socket.on('typing', function (data) {
        createChatMessage('is typing ...', data.user, data.img, moment(), true);
    });

    // // Whenever the server emits 'stop typing', kill the typing message
    socket.on('stop typing', function (data) {
        removeChatTyping(data);
    });


// end is typing


    // on connection to server get the id of person's room
    socket.on('connect', function () {
        // console.log(res);

        socket.emit('load', id);
    });
    socket.on('dep', function (data) {
        if($('#dep option').length < data.length + 1) {
            $.each(data, function (i, item) {
                dep.append($('<option>', {
                    value: item.id,
                    text: item.name
                }));
            });
        }
        // for (var i = 0; i < data.length; i++) {
        //     // console.log(JSON.stringify(data[i]));
        //     items += JSON.stringify(data[i]);
        // }
        // console.log(JSON.stringify(data));
        // console.log(items);

    });
    // var testArray = items;
    // console.log(testArray);


    // save the gravatar url
    socket.on('img', function (data) {
        img = data;
    });

    // receive the names and avatars of all people in the chat room
    socket.on('peopleinchat', function (data) {

        if (data.number === 0) {

            showMessage("connected");

            loginForm.on('submit', function (e) {

                e.preventDefault();

                name = $.trim(yourName.val());

                department = dep.val();

                if (name.length < 1) {
                    alert("Please enter a nick name longer than 1 character!");
                    return;
                }

                email = yourEmail.val();

                if (!isValid(email)) {
                    alert("Please enter a valid email!");
                }
                else {

                    showMessage("inviteSomebody");
                    connected = true;

                    // call the server-side function 'login' and send user's parameters
                    socket.emit('login', {user: name, avatar: email, id: id, dep: department});

                }

            });
        }

        else if (data.number === 1) {

            // Check If user has the cookie ro not (is user Admin or not)
            //FIXME uncomment cookie
            // if (getCookie('name') === 'test') {

            showMessage("personinchat", data);

            socket.emit('admin entered chat');

            loginForm.on('submit', function (e) {

                e.preventDefault();

                name = $.trim(hisName.val());

                // if(name.length < 1){
                // 	alert("Please enter a nick name longer than 1 character!");
                // 	return;
                // }
                //
                // if(name == data.user){
                // 	alert("There already is a \"" + name + "\" in this room!");
                // 	return;
                // }
                // email = hisEmail.val();
                //
                // if(!isValid(email)){
                // 	alert("Wrong e-mail format!");
                // }
                // else {
                socket.emit('login', {user: name, avatar: email, id: id});
                // console.log(connected + 'connnected');

                // }

            });
            // }
            // else {
            // 	// console.log('not admin');
            // alert('only admins can visit this page');
            //
            //
            // }

        }

        else {
            showMessage("tooManyPeople");
        }

    });


    // Other useful

    socket.on('startChat', function (data) {
        if (data.boolean && data.id == id) {

            // chats.empty();
            chats.css('display', 'block');
            if (name === data.users[0]) {

                showMessage("youStartedChatWithNoMessages", data);
            }
            else {

                showMessage("heStartedChatWithNoMessages", data);
            }

            chatNickname.text(friend);
        }
    });

    socket.on('leave', function (data) {

        if (data.boolean && id == data.room) {
            socket.emit('mailText', mailText);

            // console.log(hisEmail.val() + ' : ' + yourEmail.val());
            // console.log(mailText);
            // $( "html" ).remove();

            showMessage("somebodyLeft", data);
            chats.empty();
        }

    });

    socket.on('tooMany', function (data) {

        if (data.boolean && name.length === 0) {

            showMessage('tooManyPeople');
        }
    });

    socket.on('receive', function (data) {

        showMessage('chatStarted');
        console.log(data.msg);
        if (data.msg.trim().length) {
            createChatMessage(data.msg, data.user, data.img, moment(), false);
            scrollToBottom();
        }
    });

    textarea.keypress(function (e) {

        // Submit the form on enter

        if (e.which == 13) {
            e.preventDefault();
            chatForm.trigger('submit');
        }

    });

    chatForm.on('submit', function (e) {

        e.preventDefault();

        // Create a new chat message and display it directly

        showMessage("chatStarted");

        if (textarea.val().trim().length) {
            createChatMessage(textarea.val(), name, img, moment(), false);
            scrollToBottom();


            // Send the message to the other person in the chat
            socket.emit('msg', {msg: textarea.val(), user: name, img: img});

        }
        // Empty the textarea
        textarea.val("");
    });

    // Update the relative time stamps on the chat messages every minute

    setInterval(function () {

        messageTimeSent.each(function () {
            var each = moment($(this).data('time'));
            $(this).text(each.fromNow());
        });

    }, 60000);

    // Function that creates a new chat message

    function createChatMessage(msg, user, imgg, now, typing) {
        var typingClass = typing ? ' typing ' : '';
        var who = '';


        if ($.trim(msg) == '') {
            return false;
        }
        // $('<div class="message message-personal">' + msg + '</div>').appendTo($('.chats')).addClass('new');

        if (user === name) {
            //me
            who = 'message message-personal new';
            var li = $(
                '<div class="' + who + '" >' +
                // '<figure class="avatar">' +
                // '<img src=' + imgg + ' />' +
                '<b></b>' +
                '<i class="timesent" data-time=' + now + '></i> ' +
                // '</figure>' +
                '<p style="margin:0px;overflow-wrap:break-word;max-width: 100px;"></p>' +
                '</div>').addClass(typingClass).data('user', user);
        }
        else {
            //you

            who = 'message new';
            var li = $(
                '<div class="' + who + '" >' +
                '<figure class="avatar">' +
                // '<img src=' + imgg + '/>' +
                '<img src="' + imgg + '"/>' +
                '</figure>' +
                '<b></b>' +
                '<i class=" timestamp timesent" data-time=' + now + '></i> ' +
                '<p style="margin:0px;overflow-wrap:break-word;max-width: 100px;"></p>' +
                '</div>').addClass(typingClass).data('user', user);
        }


        // use the 'text' method to escape malicious user input
        if (li) {
            li.find('b').text(user);
            li.find('p').text(msg);

            chats.append(li);
        }
        // mailText += user + ' : ' + msg + '  , ';
        //
        //
        // messageTimeSent = $(".timesent");
        // messageTimeSent.last().text(now.fromNow());
    }

    function scrollToBottom() {
        // $("html, body").animate({ scrollTop: $(".chatscreen").height()-$(window).height() },1000);

        var scr = $('.chat')[0].scrollHeight;
        $('.chat').animate({scrollTop: scr}, 1000);
        // $('.chats').animate({
        //     scrollTop: $('.chats')[0].scrollHeight}, 2000);
    }

    function isValid(thatemail) {

        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(thatemail);
    }

    function showMessage(status, data) {

        if (status === "connected") {

            section.children().css('display', 'none');
            onConnect.fadeIn(1200);
        }

        else if (status === "inviteSomebody") {

            // Set the invite link content

            // $("#link").text(window.location.href);
            $("#link").text('plz wait!');

            onConnect.fadeOut(1200, function () {
                inviteSomebody.fadeIn(1200);
            });
        }

        else if (status === "personinchat") {

            onConnect.css("display", "none");
            personInside.fadeIn(1200);

            chatNickname.text(data.user);
            ownerImage.attr("src", data.avatar);
        }

        else if (status === "youStartedChatWithNoMessages") {

            left.fadeOut(1200, function () {
                inviteSomebody.fadeOut(1200, function () {
                    noMessages.fadeIn(1200);
                    footer.fadeIn(1200);
                });
            });

            friend = data.users[1];
            noMessagesImage.attr("src", data.avatars[1]);
        }

        else if (status === "heStartedChatWithNoMessages") {

            personInside.fadeOut(1200, function () {
                noMessages.fadeIn(1200);
                footer.fadeIn(1200);
            });

            friend = data.users[0];
            noMessagesImage.attr("src", data.avatars[0]);
        }

        else if (status === "chatStarted") {

            section.children().css('display', 'none');
            chatScreen.css('display', 'block');
        }

        else if (status === "somebodyLeft") {

            leftImage.attr("src", data.avatar);
            leftNickname.text(data.user);

            section.children().css('display', 'none');
            footer.css('display', 'none');
            left.fadeIn(1200);
        }

        else if (status === "tooManyPeople") {

            section.children().css('display', 'none');
            tooManyPeople.fadeIn(1200);
        }
    }

    // test select

    // test select

    // Get Cookie of given name
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});
