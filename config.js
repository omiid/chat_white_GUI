var express = require('express');
var cookieParser = require('cookie-parser');

module.exports = function(app, io){

    app.use(cookieParser());

	// Set .html as the default template extension
	app.set('view engine', 'html');

	// Initialize the ejs template engine
	app.engine('html', require('ejs').renderFile);

	// Tell express where it can find the templates
	app.set('views', __dirname + '/views');

	// Make the files in the public folder available to the world
	app.use(express.static(__dirname + '/public'));

	// Error handler
    // app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));

	// Set header to all request
    app.use(function(req, res, next) {
        res.setHeader("testHeader", "*");
        return next();
    });
};
